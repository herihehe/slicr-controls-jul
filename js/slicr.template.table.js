;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateTable = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};


        // Basic template for tables
        _nodes.table = this.renderNodeTable({
            id : "table-"+options.id
        }); 

        _nodes.thead = this.renderNodeThead(),
        _nodes.tfoot = this.renderNodeTfoot(),
        _nodes.tbody = this.renderNodeTbody(); 

        for(var i = 0; i < options.tableHead.length; i++){
            for(var theadRow = this.renderNodeTr(),j = 0; j < options.tableHead[i].length; j++){
                var headColumn = this.renderNodeTh({
                    'rowspan' : options.tableHead[i][j].rowspan || '',
                    'colspan' : options.tableHead[i][j].colspan || '',
                    'data-sorter' : options.tableHead[i][j].sortable || 'false',
                    'class' : options.tableHead[i][j].sortable ? 'tablesorter-headerUnsorted' : ''
                }); 

                headColumn.innerHTML = options.tableHead[i][j].content;
                theadRow.appendChild(headColumn);
            }

            _nodes.thead.appendChild(theadRow)
        }

        if(options.tableFoot && options.tableFoot.length>0){
            for(var i = 0; i < options.tableFoot.length; i++){
                for(var tfootRow = this.renderNodeTr(),j = 0; j < options.tableFoot[i].length; j++){
                    var footColumn = this.renderNodeTd({
                        'rowspan' : options.tableFoot[i][j].rowspan || '',
                        'colspan' : options.tableFoot[i][j].colspan || ''
                    }); 
                    footColumn.innerHTML = options.tableFoot[i][j].content;
                    tfootRow.appendChild(footColumn);
                }

                _nodes.tfoot.appendChild(tfootRow)
            }
        }

        for(var i = 0; i < options.tableBody.length; i++){
            for(var tbodyRow = this.renderNodeTr(),j = 0; j < options.tableBody[i].length; j++){
                var bodyColumn = this.renderNodeTd({
                    'rowspan' : options.tableBody[i][j].rowspan || '',
                    'colspan' : options.tableBody[i][j].colspan || '',
                    'class' : options.tableBody[i][j].isLabel ? 'is-label' : ''
                }); 
                bodyColumn.innerHTML = options.tableBody[i][j].content,
                tbodyRow.appendChild(bodyColumn);
            }

            _nodes.tbody.appendChild(tbodyRow)
        }

        options.tableHead && options.tableHead.length > 0 && _nodes.table.appendChild(_nodes.thead),
        options.tableFoot && options.tableFoot.length > 0 && _nodes.table.appendChild(_nodes.tfoot),
        options.tableBody && options.tableBody.length > 0 && _nodes.table.appendChild(_nodes.tbody),


        // Insert the table

        self.appendChild( _nodes.table );


        /* ----------------------------------------------------------
         * Build the table stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';


        // Basic stylings

        css +=  '\n#table-' + options.id + '{\n'+
                    'border-collapse: collapse;\n'+
                    'border-spacing: 0;\n'+
                '}';


        // User defined stylings

        css += '#table-' + options.id + '{ ' + rules[0] + '\n}';
        
        if( rules['head'] ) css += '#table-' + options.id + ' thead th{ ' + rules['head'][0] + '\n}';
        if( rules['head'] && rules['head']['unsorted'] ) css += '#table-' + options.id + ' thead th.tablesorter-headerUnsorted{ ' + rules['head']['unsorted'][0] + '\n}';
        if( rules['head'] && rules['head']['sort-asc'] ) css += '#table-' + options.id + ' thead th.tablesorter-headerAsc{ ' + rules['head']['sort-asc'][0] + '\n}';
        if( rules['head'] && rules['head']['sort-desc'] ) css += '#table-' + options.id + ' thead th.tablesorter-headerDesc{ ' + rules['head']['sort-desc'][0] + '\n}';
        if( rules['foot'] ) css += '#table-' + options.id + ' tfoot td{ ' + rules['foot'][0] + '\n}';
        if( rules['body'] ) css += '#table-' + options.id + ' tbody td{ ' + rules['body'][0] + '\n}';
        if( rules['body'] && rules['body']['even'] ) css += '#table-' + options.id + ' tbody tr:nth-child(even) td{ ' + rules['body']['even'] + '\n}';
        if( rules['body'] && rules['body']['label'] ) css += '#table-' + options.id + ' tbody tr td.is-label{ ' + rules['body']['label'] + '\n}';
        
        if( rules['head'] && rules['head']['cells'] ){
            for( var i = 0; i < rules['head']['cells'].length; i++ ){
                css += '#table-' + options.id + ' thead th:nth-child(' + ( i + 1 ) + '){ ' + rules['head']['cells'][i] + '\n}';
            }
        }

        if( rules['foot'] && rules['foot']['cells'] ){
            for( var i = 0; i < rules['foot']['cells'].length; i++ ){
                css += '#table-' + options.id + ' tfoot td:nth-child(' + ( i + 1 ) + '){ ' + rules['foot']['cells'][i] + '\n}';
            }
        }

        if( rules['body'] && rules['body']['cells'] ){
            for( var i = 0; i < rules['body']['cells'].length; i++ ){
                css += '#table-' + options.id + ' tbody td:nth-child(' + ( i + 1 ) + '){ ' + rules['body']['cells'][i] + '\n}';
            }
        }
        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);


        // initiate jQuery table sorter plugin
        jQuery('#table-' + options.id).tablesorter();
    };
})();