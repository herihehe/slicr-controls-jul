;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateDropdown = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};
        
        _nodes.dropdown = {};
        
        _nodes.dropdown.core = this.renderNodeDiv({
            'id': 'dropdown-' + options.id
        });
        
        _nodes.dropdown.button = this.renderNodeLink({
            'href' : options.button && options.button.value ? options.button.value : '#',
            'class' : 'dropdown-button' + ( options.button && options.button.trigger ? ' dropdown-trigger' : '' )
        });
        
        _nodes.dropdown.text = this.renderNodeSpan({
            'class' : 'dropdown-text' + ( options.text && options.text.trigger ? ' dropdown-trigger' : '' )
        });
        
        _nodes.dropdown.container = this.renderNodeDiv({
            'class' : 'dropdown-container'
        });
        
        _nodes.dropdown.icon = this.renderNodeSpan({
            'class' : 'dropdown-icon' + ( options.icon && options.icon.trigger ? ' dropdown-trigger' : '' )
        });
        
        _nodes.dropdown.menu = this.renderNodeDiv({
            'class' : 'dropdown-menu'
        });

        _nodes.dropdown.button.innerHTML = options.button && options.button.label ? options.button.label : 'button';
        _nodes.dropdown.text.innerHTML = options.text && options.text.label ? options.text.label : 'text';
        _nodes.dropdown.icon.innerHTML = options.icon && options.icon.label ? options.icon.label : ' ';

        for( var i = 0; i < options.items.length; i++){
            var dropdownMenuItem = this.renderNodeLink({
                'class' : 'dropdown-menu-item',
                'href' : options.items[i].value
            });

            dropdownMenuItem.innerHTML = options.items[i].label;
            _nodes.dropdown.menu.appendChild( dropdownMenuItem );
        }

<<<<<<< HEAD
        _nodes.dropdown.button.innerHTML = options.button && options.button.label ? options.button.label : 'button';
        _nodes.dropdown.trigger.innerHTML = options.trigger && options.trigger.label ? options.trigger.label : 'open dropdown';

        _nodes.dropdown.container.appendChild( _nodes.dropdown.trigger );
=======
        if( options.icon ) _nodes.dropdown.container.appendChild( _nodes.dropdown.icon );
        if( options.button ) _nodes.dropdown.container.appendChild( _nodes.dropdown.button );
        if( options.text ) _nodes.dropdown.container.appendChild( _nodes.dropdown.text );
>>>>>>> 289c75b279cf1843274b581269471573aac21946
        _nodes.dropdown.container.appendChild( _nodes.dropdown.menu );
        _nodes.dropdown.core.appendChild( _nodes.dropdown.container );


        // Insert the input slider

        self.appendChild( _nodes.dropdown.core );

        /* ----------------------------------------------------------
         * Build the dropdown stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);

        css = '';
        
        // Basic stylings
        css +=  '\n#dropdown-' + options.id + ' {'+
                '\n    list-style: none;'+
                '\n    padding: 0;'+
                '\n    margin: 0;'+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-button { '+
                '\n    display: none; '+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-text { '+
                '\n    display: none; '+
                '\n    cursor: pointer; '+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-container { '+
                '\n    position: relative; '+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-icon { '+
                '\n    cursor: pointer; '+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-menu {'+
                '\n    display: none;'+
                '\n    position: absolute;'+
                '\n    top: 100%;'+
                '\n    left: 0;'+
                '\n    z-index: 1000;'+
                '\n    display: none;'+
                '\n    min-width: 150px;'+
                '\n    list-style: none;'+
                '\n    padding: 0;'+
                '\n    margin: 0;'+
                '\n}\n';
                
        css +=  '\n#dropdown-' + options.id + ' .dropdown-open .dropdown-menu { '+
                '\n    display: block; '+
                '\n}\n';

        css +=  '\n#dropdown-' + options.id + ' .dropdown-menu .dropdown-menu-item {'+
                '\n    display: block;'+
                '\n}\n';

        css +=  '\n#dropdown-' + options.id + ' .dropdown-menu .dropdown-menu-item:hover {'+
                '\n    text-decoration: none;'+
                '\n}\n';


        // User defined stylings
        css += '#dropdown-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['button'] ) css += '\n#dropdown-' + options.id + ' .dropdown-button {\n'+ rules['button'][0] +'\n}\n';
        if( rules['button'] && rules['button']['hover'] ) css += '\n#dropdown-' + options.id + ' .dropdown-button:hover {\n'+ rules['button']['hover'][0] +'\n}\n';
        if( rules['text'] ) css += '\n#dropdown-' + options.id + ' .dropdown-text {\n'+ rules['text'][0] +'\n}\n';
        if( rules['container'] ) css += '\n#dropdown-' + options.id + ' .dropdown-container {\n'+ rules['container'][0] +'\n}\n';
        if( rules['icon'] ) css += '\n#dropdown-' + options.id + ' .dropdown-icon {\n'+ rules['icon'][0] +'\n}\n';
        if( rules['icon'] && rules['icon']['open'] ) css += '\n#dropdown-' + options.id + ' .dropdown-open .dropdown-icon {\n'+ rules['icon']['open'][0] +'\n}\n';
        if( rules['menu'] ) css += '\n#dropdown-' + options.id + ' .dropdown-menu {\n'+ rules['menu'][0] +'\n}\n';
        if( rules['menu'] && rules['menu']['item'] ) css += '\n#dropdown-' + options.id + ' .dropdown-menu .dropdown-menu-item {\n'+ rules['menu']['item'][0] +'\n}\n';
        if( rules['menu'] && rules['menu']['item'] && rules['menu']['item']['hover'] ) css += '\n#dropdown-' + options.id + ' .dropdown-menu .dropdown-menu-item:hover {\n'+ rules['menu']['item']['hover'][0] +'\n}\n';
      

        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);

        /* --------------------------------
         * Custom dropdown function
         * using dropdown plugin
         * -------------------------------- */
        
        jQuery('#dropdown-' + options.id).dropdown();
        
    };
})();