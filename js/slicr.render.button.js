;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeButton = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeButton;
            
        // Basic template for button
        nodeButton = document.createElement('button');
        
        // Read user specified settings
        this.readSetting(options, nodeButton);

        return nodeButton;
    };
})();