var slicrMethod = function(){
    this.extend = function() {
        for (var i = 1; i < arguments.length; i++) {
            for (var key in arguments[i]) {
                if (arguments[i].hasOwnProperty(key)) {
                    arguments[0][key] = arguments[i][key];
                }
            }
        }
        return arguments[0];
    };

    this.setAttributes = function(el, attrs) {
        for (var key in attrs) {
            el.setAttribute(key, attrs[key]);
        }
    };

    this.getUID = function() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };

    this.readSetting = function(setting, element){
        for( var prop in setting){
            switch(prop){
                case 'text':
                    var text = setting['multiple-background'] ? '<span>' + setting[prop] + '</span>' : setting[prop];
                    element.innerHTML = text;
                    break;

                case 'multiple-background':
                    break;

                case 'disabled':
                    if( setting[prop] === true ) element.setAttribute(prop, setting[prop]);
                    break;

                default:
                    element.setAttribute(prop, setting[prop]);
                    break;
            }
        }
    };

    this.optionsToCss = function(obj) {
        var rules = [],
            css3Rules = {
                boxShadow: {
                    color: null,
                    pixels: null
                },
                linearGradient: {
                    colorStart: null,
                    colorStop: null,
                    direction: null
                },
                borderRadius: 0
            };

        // Define string type for normal state rules
        rules[0] = '';

        /* ------------------------------------------
         * Iterate each properties, and save them 
         * as a normal rules for any non-object,
         * otherwise, re-iterate them as state rules
         * ------------------------------------------ */
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                var v = obj[i];
                
                // ignore null values
                if (v === null) return;
                
                // state rules: hover, focus, disabled
                else if (typeof v == 'object') {
                    rules[i] = this.optionsToCss(v);
                }
                
                // normal rules
                else {
                    
                    /* ----------------------------------------------
                     * Literal CSS Properties:
                     * this is where the function is converting
                     * each styles option into some valid CSS rules
                     * ---------------------------------------------- */
                    switch (i) {
                        case 'shade-color':
                            if (v !== 'none') css3Rules.boxShadow.color = v;
                            break;
                        
                        case 'shade-pixels':
                            if (v !== 0) css3Rules.boxShadow.pixels = v;
                            break;
                        
                        case 'gradient-color-start':
                            if (v !== 0) css3Rules.linearGradient.colorStart = v;
                            break;
                        
                        case 'gradient-color-stop':
                            if (v !== 0) css3Rules.linearGradient.colorStop = v;
                            break;
                        
                        case 'gradient-direction':
                            if (v !== 0) css3Rules.linearGradient.direction = v;
                            break;
                        
                        case 'border-radius':
                            if (v !== 0) css3Rules.borderRadius = v;
                            break;
                        
                        // some options are just plain CSS rules, 
                        // no need to convert
                        default:
                            rules[0] += '\n\t' + i + ': ' + v + ';';
                            break;
                    }
                }
            }
        }
        
        // Process CSS3 rules
        for (var i in css3Rules) {
            if (css3Rules.hasOwnProperty(i)) {
                var val = css3Rules[i];
                
                // ignore null values
                if (val === null) return;

                switch (i) {
                    case 'boxShadow':
                        if (val.color !== null) {
                            rules[0] += '\n\t-webkit-box-shadow: ' + val.pixels + ' ' + val.color + ';';
                            rules[0] += '\n\t-moz-box-shadow: ' + val.pixels + ' ' + val.color + ';';
                            rules[0] += '\n\tbox-shadow: ' + val.pixels + ' ' + val.color + ';';
                        }
                        break;
                        
                    case 'linearGradient':
                        if (val.colorStart !== null) {
                            var directions = {
                                webkitOld: val.direction == 'vertical' ? 'left top, left bottom' : 'top left, top right',
                                webkitNew: val.direction == 'vertical' ? 'to bottom' : 'to right',
                                global: val.direction == 'vertical' ? 'top' : 'left',
                                ie: val.direction == 'vertical' ? 0 : 1
                            };
                            rules[0] += '\n\tbackground: -webkit-gradient(linear, ' + directions.webkitOld + ', color-stop(0%,' + val.colorStart + '), color-stop(100%,' + val.colorStop + '));';
                            rules[0] += '\n\tbackground: -webkit-linear-gradient(' + directions.global + ', ' + val.colorStart + ' 0%,' + val.colorStop + ' 100%);';
                            rules[0] += '\n\tbackground: -moz-linear-gradient(' + directions.global + ', ' + val.colorStart + ' 0%, ' + val.colorStop + ' 100%);';
                            rules[0] += '\n\tbackground: linear-gradient(' + directions.webkitNew + ', ' + val.colorStart + ' 0%,' + val.colorStop + ' 100%);';
                            rules[0] += '\n\tfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'' + val.colorStart + '\', endColorstr=\'' + val.colorStop + '\',GradientType=' + directions.ie + ' );;';
                        }
                        break;
                        
                    case 'borderRadius':
                        if (val !== 0) {
                            rules[0] += '\n\tborder-radius: ' + val + ';';
                            rules[0] += '\n\t-webkit-border-radius: ' + val + ';';
                            rules[0] += '\n\t-moz-border-radius: ' + val + ';';
                        }
                        break;
                }
            }
        }
            
        return rules;
    };
}, slicrControl, $;

/* --------------------------------------
 * Add a simple wrapper to the methods;
 * P.S.: it was initially coded in jQuery 
 * -------------------------------------- */
slicrControl = function(selector) {
    this.node = document.querySelector(selector);
};

slicrControl.prototype = new slicrMethod();

$ = function(selector) {
    return new slicrControl(selector);
};