;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateProgressBar = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {},
                value: 0
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};
        
        _nodes.progress = {};
        _nodes.progress.container = this.renderNodeDiv({
            'id': 'progress-' + options.id,
            'class': 'contain-progress'
        });

        _nodes.progress.value = this.renderNodeDiv({
            'class': 'progress-value'
        });

        _nodes.progress.bar = this.renderNodeDiv({
            'class': 'progress-bar'
        });

        _nodes.progress.current = this.renderNodeDiv({
            'class': 'progress-bar-current',
            'style': 'width: ' + options.value + '%;'
        });

        _nodes.progress.value.innerHTML = options.value + '%';
        _nodes.progress.bar.appendChild( _nodes.progress.current );
        _nodes.progress.container.appendChild( _nodes.progress.value );
        _nodes.progress.container.appendChild( _nodes.progress.bar );


        // Insert the progress bar

        self.appendChild( _nodes.progress.container );

        /* ----------------------------------------------------------
         * Build the progress bar stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        // Put any basic progress bar elements' stylings here


        // User defined stylings
        css += '#progress-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['value'] ) css += '\n#progress-' + options.id + ' .progress-value {\n'+ rules['value'][0] +'\n}\n';
        if( rules['bar'] ) css += '\n#progress-' + options.id + ' .progress-bar {\n'+ rules['bar'][0] +'\n}\n';
        if( rules['bar'] && rules['bar']['current'] ) css += '\n#progress-' + options.id + ' .progress-bar-current {\n'+ rules['bar']['current'][0] +'\n}\n';
        
        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);
    };
})();