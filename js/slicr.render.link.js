;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeLink = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeLink;
            
        // Basic template for link
        nodeLink = document.createElement('a');
        
        // Read user specified settings
        this.readSetting(options, nodeLink);

        return nodeLink;
    };
})();