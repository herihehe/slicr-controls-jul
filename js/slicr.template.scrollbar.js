;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateScrollBar = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};

        _nodes.scrollbar = this.renderNodeDiv({
            'id': 'scrollbar-' + options.id
        });

        _nodes.content = this.renderNodeDiv({
            'class': 'scrollbar-content'
        });

        _nodes.scrollbar.appendChild( _nodes.content );


        // Insert the scrollbar

        self.appendChild( _nodes.scrollbar );

        /* ----------------------------------------------------------
         * Build the scrollbar bar stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        css +=  '#scrollbar-' + options.id + '{\n'+
                '   overflow : auto;\n'+
                '}\n';


        // User defined stylings
        css += '#scrollbar-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['content'] ) css += '\n#scrollbar-' + options.id + ' .scrollbar-content {'+ rules['content'][0] +'\n}\n';
        if( rules['bar'] ) css += '\n#scrollbar-' + options.id + '::-webkit-scrollbar {'+ rules['bar'][0] +'\n}\n';
        if( rules['track'] ) css += '\n#scrollbar-' + options.id + '::-webkit-scrollbar-track {'+ rules['track'][0] +'\n}\n';
        if( rules['thumb'] ) css += '\n#scrollbar-' + options.id + '::-webkit-scrollbar-thumb {'+ rules['thumb'][0] +'\n}\n';

        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);
    };
})();