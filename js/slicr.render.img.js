;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeImg = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeImg;
            
        // Basic template for image
        nodeImg = document.createElement('img');
        
        // Read user specified settings
        this.readSetting(options, nodeImg);

        return nodeImg;
    };
})();