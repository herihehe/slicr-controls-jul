;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeSpan = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeSpan;
            
        // Basic template for span
        nodeSpan = document.createElement('span');
        
        // Read user specified settings
        this.readSetting(options, nodeSpan);

        return nodeSpan;
    };
})();