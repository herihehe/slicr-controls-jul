;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeDiv = function(settings) {
        var defaults = {
                id: this.getUID()
            },
            options = this.extend({}, defaults, settings),
            nodeDiv;
            
        // Basic template for div
        nodeDiv = document.createElement('div');
        
        // Read user specified settings
        this.readSetting(options, nodeDiv);

        return nodeDiv;
    };
})();