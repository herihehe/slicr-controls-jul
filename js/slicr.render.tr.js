;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTr = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTr;
            
        // Basic template for Tr
        nodeTr = document.createElement('tr');
        
        // Read user specified settings
        this.readSetting(options, nodeTr);

        return nodeTr;
    };
})();