;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeLi = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeLink;
            
        // Basic template for list item
        nodeLink = document.createElement('li');
        
        // Read user specified settings
        this.readSetting(options, nodeLink);

        return nodeLink;
    };
})();