/*
 * dropdown v1.1.0
 * http://dev7studios.com/dropdown
 *
 * Copyright 2012, Dev7studios
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

;(function($) {

    $.fn.dropdown = function(method) {

        var methods = {

            init : function(options) {
                this.dropdown.settings = $.extend({}, this.dropdown.defaults, options);
                return this.each(function() {
                    var $el = $(this),
                         el = this,
                         settings = $.fn.dropdown.settings;

                    // Hide initial submenus
                    $el.find('>'+ settings.triggerParentEl +':has('+ settings.submenuEl +')')
                        .find(settings.submenuEl)
                            .hide();

                    // Open on click
                    $el.off(settings.action).on(settings.action, settings.triggerParentEl +':has('+ settings.submenuEl +') > '+ settings.triggerEl +'', function(){
                        // Close click menu's if clicked again
                        if(settings.action == 'click' && $(this).parents(settings.triggerParentEl).hasClass('dropdown-open')){
                            $(this)
                                .parents(settings.triggerParentEl)
                                    .removeClass('dropdown-open')
                                    .find(settings.submenuEl)
                                        .hide();

                            return false;
                        }

                        // Hide open menus
                        $('.dropdown-open')
                            .removeClass('dropdown-open')
                            .find('.dropdown-menu')
                                .hide();

                        // Open this menu
                        $(this)
                            .parents(settings.triggerParentEl)
                                .addClass('dropdown-open')
                                .find(settings.submenuEl)
                                    .show();

                        return false;
                    });

                    // Close if outside click
                    $(document).on('click', function(){
                        $('.dropdown-open')
                            .removeClass('dropdown-open')
                            .find('.dropdown-menu')
                                .hide();
                    });
                });
            }

        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error( 'Method "' +  method + '" does not exist in dropdown plugin!');
        }

    };

    $.fn.dropdown.defaults = {
        action: 'click', // The open action for the trigger
        submenuEl: '.dropdown-menu', // The submenu element
        triggerEl: '.dropdown-trigger', // The trigger element
        triggerParentEl: '.dropdown-container', // The trigger parent element
    };

    $.fn.dropdown.settings = {};

})(jQuery);