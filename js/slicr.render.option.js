;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeOption = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeOption;
            
        // Basic template for Option
        nodeOption = document.createElement('option');
        
        // Read user specified settings
        this.readSetting(options, nodeOption);

        return nodeOption;
    };
})();