;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTh = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTh;
            
        // Basic template for Th
        nodeTh = document.createElement('th');
        
        // Read user specified settings
        this.readSetting(options, nodeTh);

        return nodeTh;
    };
})();