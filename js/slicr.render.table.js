;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTable = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTable;
            
        // Basic template for Table
        nodeTable = document.createElement('table');
        
        // Read user specified settings
        this.readSetting(options, nodeTable);

        return nodeTable;
    };
})();