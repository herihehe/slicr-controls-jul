;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateTab = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};

        _nodes.tab = this.renderNodeDiv({
            'id' : 'tab-' + options.id
        });


        // Insert the tab

        self.appendChild( _nodes.tab );


        /* Render tab menu item
         * It needs to be called after the tab container
         * is available in the DOM
         */
        _nodes.menu = $( '#tab-' + options.id ).templateMenuBar( options.menubar );

        // Dynamically set the menu tab links
        for( var i = 0; i < _nodes.menu.children.length; i++ ){
            this.setAttributes( _nodes.menu.children[i].children[0], {
                'href' : '#tab-pane-' + options.id + '-' + i
            });
        }

        /* Render tab pane item
         * It needs to be called after the tab container
         * is available in the DOM
         */
        for( var i = 0; i < options.panes.length; i++ ){
            var pane = this.renderNodeDiv({
                'class' : 'tab-pane',
                'id' : 'tab-pane-' + options.id + '-' + i
            });

            pane.innerHTML = options.panes[i].content;
            _nodes.tab.appendChild( pane );
        }


        /* ----------------------------------------------------------
         * Build the tab stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css +=  '\n#tab-' + options.id + ' .tab-pane:not(:first-of-type){\n'+
                '   display: none;\n'+
                '}';

        // User defined stylings
        css += '#tab-' + options.id + '{ ' + rules[0] + '\n}';
        if( rules['pane'] ) css += '#tab-' + options.id + ' .tab-pane { ' + rules['pane'][0] + '\n}';
        
        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);


        // Initiate jQuery UI tabs function
        jQuery( '#tab-' + options.id ).tabs();
    };
})();