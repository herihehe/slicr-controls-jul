;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeUl = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeLink;
            
        // Basic template for unordered list
        nodeLink = document.createElement('ul');
        
        // Read user specified settings
        this.readSetting(options, nodeLink);

        return nodeLink;
    };
})();