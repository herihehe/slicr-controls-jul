;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeInput = function(settings) {
        var defaults = {
                id: this.getUID()
            },
            options = this.extend({}, defaults, settings),
            nodeInput;
            
        // Basic template for input
        nodeInput = document.createElement('input');
        
        // Read user specified settings
        this.readSetting(options, nodeInput);

        return nodeInput;
    };
})();