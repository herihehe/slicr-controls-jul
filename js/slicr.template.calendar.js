;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateCalendar = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};
        
        _nodes.calendar = this.renderNodeDiv({
            'id' : 'calendar-' + options.id
        });


        /*
         * calendar pre-rendered template;
         * this is the basic template to mimic exactly
         * what will be rendered after the jQuery plugin call
         */

        var _pre = {};
        _pre.days = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
        _pre.months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        _pre.calendar = new Date();
        _pre.year = _pre.calendar.getFullYear();
        _pre.month = _pre.calendar.getMonth();
        _pre.today = _pre.calendar.getDate();
        _pre.weekday = _pre.calendar.getDay();
        _pre.daysOfMonth = 31;
        _pre.daysOfWeek = 7;
        _pre.tbodyRowIndex = 0;

        _pre.calendar.setDate(1);

        _nodes.pre = {}
        _nodes.pre.calendar = this.renderNodeDiv({
            'class' : 'ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'
        });
        _nodes.pre.header = this.renderNodeDiv({
            'class' : 'ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all'
        });
        _nodes.pre.prev = this.renderNodeLink({
            'class' : 'ui-datepicker-prev ui-corner-all'
        });
        _nodes.pre.prevSpan = this.renderNodeSpan({
            'class' : 'ui-icon ui-icon-circle-triangle-w'
        });
        _nodes.pre.next = this.renderNodeLink({
            'class' : 'ui-datepicker-next ui-corner-all'
        });
        _nodes.pre.nextSpan = this.renderNodeSpan({
            'class' : 'ui-icon ui-icon-circle-triangle-e'
        });
        _nodes.pre.title = this.renderNodeDiv({
            'class' : 'ui-datepicker-title'
        });
        _nodes.pre.month = this.renderNodeSelect({
            'class' : 'ui-datepicker-month'
        });
        _nodes.pre.year = this.renderNodeSelect({
            'class' : 'ui-datepicker-year'
        });
        _nodes.pre.table = this.renderNodeTable({
            'class' : 'ui-datepicker-calendar'
        });
        _nodes.pre.monthOption = this.renderNodeOption();
        _nodes.pre.yearOption = this.renderNodeOption();
        _nodes.pre.thead = this.renderNodeThead();
        _nodes.pre.theadRow = this.renderNodeTr();
        _nodes.pre.tbody = this.renderNodeTbody();
        _nodes.pre.tbodyRow = [];
        _nodes.pre.tbodyRow[0] = this.renderNodeTr();

        _nodes.pre.monthOption.innerHTML = _pre.months[ _pre.month ];
        _nodes.pre.yearOption.innerHTML = _pre.year;
        _nodes.pre.prevSpan.innerHTML = 'prev';
        _nodes.pre.nextSpan.innerHTML = 'next';
        _nodes.pre.month.appendChild( _nodes.pre.monthOption );
        _nodes.pre.year.appendChild( _nodes.pre.yearOption );
        _nodes.pre.title.appendChild( _nodes.pre.month );
        _nodes.pre.title.appendChild( _nodes.pre.year );
        _nodes.pre.prev.appendChild( _nodes.pre.prevSpan );
        _nodes.pre.next.appendChild( _nodes.pre.nextSpan );
        _nodes.pre.header.appendChild( _nodes.pre.prev );
        _nodes.pre.header.appendChild( _nodes.pre.next );
        _nodes.pre.header.appendChild( _nodes.pre.title );

        for( index = 0; index < _pre.days.length; index++ ){
            var th = this.renderNodeTh();
            th.innerHTML = _pre.days[ index ];
            _nodes.pre.theadRow.appendChild( th );
        }

        _nodes.pre.thead.appendChild( _nodes.pre.theadRow );

         for( index = 0; index < _pre.calendar.getDay(); index++ ){
            var td = this.renderNodeTd({
                'class' : 'ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled'
            });
            td.innerHTML = '&nbsp;';
            _nodes.pre.tbodyRow[0].appendChild( td );
         }

        for( index = 0; index < _pre.daysOfMonth; index++ ){
            if( _pre.calendar.getDate() > index ){
                var week_day =_pre.calendar.getDay();
                var tr = this.renderNodeTr();
                var td = this.renderNodeTd();
                var link = this.renderNodeLink();

                //if( week_day === 0 ) _nodes.pre.tbodyRow.push( tr );

                if(week_day != _pre.daysOfWeek){
                  var day  = _pre.calendar.getDate();
                  link.innerHTML = day;
                  td.appendChild(link);
                  _nodes.pre.tbodyRow[ _pre.tbodyRowIndex ].appendChild( td );
                }

                if( week_day === _pre.daysOfWeek - 1 ){
                  _nodes.pre.tbodyRow.push( tr )
                  _pre.tbodyRowIndex++;
                }
           }

          _pre.calendar.setDate( _pre.calendar.getDate() + 1 );

        }

        for( index = 0; index <= _pre.tbodyRowIndex; index++ ){
            _nodes.pre.tbody.appendChild( _nodes.pre.tbodyRow[index] );
        }

        _nodes.pre.table.appendChild( _nodes.pre.thead );
        _nodes.pre.table.appendChild( _nodes.pre.tbody );
        _nodes.pre.calendar.appendChild( _nodes.pre.header );
        _nodes.pre.calendar.appendChild( _nodes.pre.table );
        _nodes.calendar.appendChild( _nodes.pre.calendar );


        // Insert the calendar

        self.appendChild( _nodes.calendar );

        /* ----------------------------------------------------------
         * Build the seek bar stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css +=  '#calendar-' + options.id + ' .ui-datepicker-header {\n'+
                '    position: relative;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' .ui-datepicker-prev,\n'+
                '#calendar-' + options.id + ' .ui-datepicker-next {\n'+
                '    position: absolute;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' .ui-datepicker-prev {\n'+
                '    left: 2px;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' .ui-datepicker-next {\n'+
                '    right: 2px;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' .ui-datepicker-title {\n'+
                '    margin: 0 2.3em;\n'+
                '    text-align: center;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' .ui-datepicker-title select {\n'+
                '    width: 45%;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' table {\n'+
                '    width: 100%;\n'+
                '    border-collapse: collapse;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' th {\n'+
                '    text-align: center;\n'+
                '    font-weight: bold;\n'+
                '    border: 0;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' td {\n'+
                '    border: 0;\n'+
                '}\n';

        css +=  '#calendar-' + options.id + ' td span,\n'+
                '#calendar-' + options.id + ' td a {\n'+
                '    display: block;\n'+
                '    text-decoration: none;\n'+
                '}\n';

        
        // User defined stylings
        css += '#calendar-' + options.id + ' {'+ rules[0] + '\n}\n';

        if( rules['header'] ) css +=  '#calendar-' + options.id + ' .ui-datepicker-header {'+ rules['header'][0] +'\n}\n';
        
        if( rules['nav'] ) css +=   '#calendar-' + options.id + ' .ui-datepicker-prev,\n'+
                                    '#calendar-' + options.id + ' .ui-datepicker-next {'+ rules['nav'][0] +'\n}\n';
        if( rules['nav'] && rules['nav']['prev'] ) css +=  '#calendar-' + options.id + ' .ui-datepicker-prev {'+ rules['nav']['prev'][0] +'\n}\n';
        if( rules['nav'] && rules['nav']['next'] ) css +=  '#calendar-' + options.id + ' .ui-datepicker-next {'+ rules['nav']['next'][0] +'\n}\n';

        if( rules['title'] ) css +=  '#calendar-' + options.id + ' .ui-datepicker-title {'+ rules['title'][0] +'\n}\n';
        if( rules['title'] && rules['title']['select'] ) css +=  '#calendar-' + options.id + ' .ui-datepicker-title select {'+ rules['title']['select'][0] +'\n}\n';

        if( rules['table'] ) css +=  '#calendar-' + options.id + ' table {'+ rules['table'][0] +'\n}\n';
        if( rules['table'] && rules['table']['cell'] ) css +=  '#calendar-' + options.id + ' table td {'+ rules['table']['cell'][0] +'\n}\n';
        if( rules['table'] && rules['table']['head'] && rules['table']['head']['cell'] ) css +=  '#calendar-' + options.id + ' table th {'+ rules['table']['head']['cell'][0] +'\n}\n';

        if( rules['link'] ) css +=  '#calendar-' + options.id + ' td span,\n'+
                                    '#calendar-' + options.id + ' td a {'+ rules['link'][0] +'\n}\n';


        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);

        /* --------------------------------
         * Calendar / datepicker UI 
         * using jquery UI plugin
         * -------------------------------- */

        jQuery( "#calendar-" + options.id ).empty().datepicker({
            changeMonth: true,
            changeYear: true
        });
    };
})();