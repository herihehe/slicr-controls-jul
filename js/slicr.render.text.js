;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeText = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeText;
            
        // Basic template for paragraph
        nodeText = document.createElement('p');
        
        // Read user specified settings
        this.readSetting(options, nodeText);

        return nodeText;
    };
})();