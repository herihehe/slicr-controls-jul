;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateInputSlider = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {},
                min: 0,
                max: 100,
                value: 0
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};
        
        _nodes.inputSlider = this.renderNodeDiv({
            'id': 'input-slider-' + options.id,
            'class' : 'ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'
        });

        _nodes.inputSliderRange = this.renderNodeDiv({
            'class' : 'ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min',
            'style' : 'width: ' + ( options.value / options.max * 100 ) + '%;'
        });

        _nodes.inputSliderHandle = this.renderNodeSpan({
            'class' : 'ui-slider-handle ui-state-default ui-corner-all',
            'style' : 'left: ' + ( options.value / options.max * 100 ) + '%;'
        });

        _nodes.tooltipWrapper = this.renderNodeDiv({
          'class': 'tooltip'
        });

        _nodes.tooltipInner = this.renderNodeDiv({
          'class': 'tooltip-inner'
        });

        _nodes.tooltipArrow = this.renderNodeDiv({
          'class': 'tooltip-arrow'
        });

        var sliderTooltip = function(event, ui) {
            if( ! options.tooltip ) return;

            _currentValue = typeof ui.value === 'undefined' ? options.value : ui.value;

            _nodes.tooltipInner.innerHTML = _currentValue;
            _nodes.tooltipWrapper.appendChild( _nodes.tooltipInner );
            _nodes.tooltipWrapper.appendChild( _nodes.tooltipArrow );

            jQuery('#input-slider-' + options.id + ' .ui-slider-handle').html( _nodes.tooltipWrapper );

        }

        _nodes.inputSlider.appendChild( _nodes.inputSliderRange );
        _nodes.inputSlider.appendChild( _nodes.inputSliderHandle );


        // Insert the input slider

        self.appendChild( _nodes.inputSlider );

        /* ----------------------------------------------------------
         * Build the input slider stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css +=  '\n#input-slider-' + options.id + ' {'+
                '\n    position: relative;'+
                '\n    text-align: left;'+
                '\n}\n';

        css +=  '\n#input-slider-' + options.id + ' .ui-slider-handle {'+
                '\n    position: absolute;'+
                '\n    z-index: 2;'+
                '\n    cursor: pointer;'+
                '\n    -ms-touch-action: none;'+
                '\n    touch-action: none;'+
                '\n}\n';

        css +=  '\n#input-slider-' + options.id + ' .ui-slider-range {'+
                '\n    position: absolute;'+
                '\n    z-index: 1;'+
                '\n    display: block;'+
                '\n    border: 0;'+
                '\n    background-position: 0 0;'+
                '\n    top: 0;'+
                '\n    height: 100%;'+
                '\n}\n';

        css +=  '\n#input-slider-' + options.id + ' .tooltip {'+
                '\n    position: absolute;'+
                '\n    z-index: 1020;'+
                '\n    display: block;'+
                '\n    visibility: visible;'+
                '\n    padding: 5px;'+
                '\n    font-size: 11px;'+
                '\n    margin-top: -2px;'+
                '\n    bottom: 120%;'+
                '\n    margin-left: -2em;'+
                '\n}';

        css +=  '\n#input-slider-' + options.id + ' .tooltip .tooltip-arrow {'+
                '\n    position: absolute;'+
                '\n    width: 0;'+
                '\n    height: 0;'+
                '\n    bottom: 0;'+
                '\n    left: 50%;'+
                '\n    margin-left: -5px;'+
                '\n    border-top: 5px solid #000000;'+
                '\n    border-right: 5px solid transparent;'+
                '\n    border-left: 5px solid transparent;'+
                '\n}';

        css +=  '\n#input-slider-' + options.id + ' .tooltip-inner {'+
                '\n    text-align: center;'+
                '\n    text-decoration: none;'+
                '\n    color: #ffffff;'+
                '\n    background-color: #000000;'+
                '\n    max-width: 200px;'+
                '\n    padding: 3px 8px;'+
                '\n}';


        // User defined stylings
        css += '#input-slider-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['handle'] ) css += '\n#input-slider-' + options.id + ' .ui-slider-handle {\n'+ rules['handle'][0] +'\n}\n';
        if( rules['range'] ) css += '\n#input-slider-' + options.id + ' .ui-slider-range {\n'+ rules['range'][0] +'\n}\n';
        if( rules['range-min'] ) css += '\n#input-slider-' + options.id + ' .ui-slider-range-min {\n'+ rules['range-min'][0] +'\n}\n';
        if( rules['tooltip'] ) css += '\n#input-slider-' + options.id + ' .tooltip {\n'+ rules['tooltip'][0] +'\n}\n';
        if( rules['tooltip'] && rules['tooltip']['arrow'] ) css += '\n#input-slider-' + options.id + ' .tooltip .tooltip-arrow {\n'+ rules['tooltip']['arrow'][0] +'\n}\n';
        if( rules['tooltip'] && rules['tooltip']['inner'] ) css += '\n#input-slider-' + options.id + ' .tooltip .tooltip-inner {\n'+ rules['tooltip']['inner'][0] +'\n}\n';

        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);

        /* --------------------------------
         * Custom slider UI 
         * using jquery UI plugin
         * -------------------------------- */
        
        jQuery( '#input-slider-' + options.id ).empty().slider({
          orientation: "horizontal",
          range: "min",
          animate: true,
          min: options.min,
          max: options.max,
          value: options.value,
          create: sliderTooltip,
          slide: sliderTooltip
        });
    };
})();