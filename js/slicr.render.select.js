;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeSelect = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeSelect;
            
        // Basic template for Select
        nodeSelect = document.createElement('select');
        
        // Read user specified settings
        this.readSetting(options, nodeSelect);

        return nodeSelect;
    };
})();