;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateInputRangeSlider = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {},
                min: 0,
                max: 100,
                values: null
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};
        
        _nodes.inputSlider = this.renderNodeDiv({
            'id' : 'input-range-slider-' + options.id,
            'class' : 'ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'
        });

        _nodes.inputSliderRange = this.renderNodeDiv({
            'class' : 'ui-slider-range ui-widget-header ui-corner-all',
            'style' : 'width: ' + ( ( options.values[1] - options.values[0] ) / options.max * 100 ) + '%;left: ' + ( options.values[0] / options.max * 100 ) + '%;'
        });

        _nodes.inputSliderHandleMin = this.renderNodeSpan({
            'class' : 'ui-slider-handle ui-state-default ui-corner-all',
            'style' : 'left: ' + ( options.values[0] / options.max * 100 ) + '%;'
        });

        _nodes.inputSliderHandleMax = this.renderNodeSpan({
            'class' : 'ui-slider-handle ui-state-default ui-corner-all',
            'style' : 'left: ' + ( options.values[1] / options.max * 100 ) + '%;'
        });



        _nodes.inputSlider.appendChild( _nodes.inputSliderRange );
        _nodes.inputSlider.appendChild( _nodes.inputSliderHandleMin );
        _nodes.inputSlider.appendChild( _nodes.inputSliderHandleMax );


        // Insert the input slider

        self.appendChild( _nodes.inputSlider );

        /* ----------------------------------------------------------
         * Build the input slider stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';
        
        // Basic stylings
        css +=  '\n#input-range-slider-' + options.id + ' {'+
                '\n    position: relative;'+
                '\n    text-align: left;'+
                '\n}\n';

        css +=  '\n#input-range-slider-' + options.id + ' .ui-slider-handle {'+
                '\n    position: absolute;'+
                '\n    z-index: 2;'+
                '\n    cursor: pointer;'+
                '\n    -ms-touch-action: none;'+
                '\n    touch-action: none;'+
                '\n}\n';

        css +=  '\n#input-range-slider-' + options.id + ' .ui-slider-range {'+
                '\n    position: absolute;'+
                '\n    z-index: 1;'+
                '\n    display: block;'+
                '\n    border: 0;'+
                '\n    background-position: 0 0;'+
                '\n    top: 0;'+
                '\n    height: 100%;'+
                '\n}\n';


        // user defined stylings
        css += '#input-range-slider-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['handle'] ) css += '\n#input-range-slider-' + options.id + ' .ui-slider-handle {\n'+ rules['handle'][0] +'\n}\n';
        if( rules['range'] ) css += '\n#input-range-slider-' + options.id + ' .ui-slider-range {\n'+ rules['range'][0] +'\n}\n';

        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);

        /* --------------------------------
         * Custom slider UI 
         * using jquery UI plugin
         * -------------------------------- */
        
        jQuery( '#input-range-slider-' + options.id ).empty().slider({
          orientation: "horizontal",
          animate: true,
          range: true,
          min: options.min,
          max: options.max,
          values: options.values
        });
    };
})();