;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeThead = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeThead;
            
        // Basic template for Thead
        nodeThead = document.createElement('thead');
        
        // Read user specified settings
        this.readSetting(options, nodeThead);

        return nodeThead;
    };
})();