;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTfoot = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTfoot;
            
        // Basic template for Tfoot
        nodeTfoot = document.createElement('tfoot');
        
        // Read user specified settings
        this.readSetting(options, nodeTfoot);

        return nodeTfoot;
    };
})();