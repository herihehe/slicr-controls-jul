;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTbody = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTbody;
            
        // Basic template for Tbody
        nodeTbody = document.createElement('tbody');
        
        // Read user specified settings
        this.readSetting(options, nodeTbody);

        return nodeTbody;
    };
})();