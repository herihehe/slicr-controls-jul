;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateMenuBar = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {}
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};

        _nodes.menuBar = this.renderNodeUl({
            'id': 'menu-bar-' + options.id
        });

        _nodes.children = [];

        function renderChildren(parent, items){
            var slicr = new slicrMethod;

            // render sub menu ul element
            var subListItem = slicr.renderNodeUl();

            for (var i in items) {
                if (items.hasOwnProperty(i)) {
                    var v = items[i];
                    var listItem = slicr.renderNodeLi();
                    
                    // ignore null values
                    if (v === null) return;

                    var listItemLink = slicr.renderNodeLink({
                        'href' : v.link
                    });

                    listItemLink.innerHTML = v.label;
                    listItem.appendChild( listItemLink );
                    
                    // Directly insert first level menu items to the parent UL;
                    // otherwise, insert them into the submenu UL element
                    // and append it later outside the loop
                    if( parent.nodeName === 'UL' ){
                        parent.appendChild(listItem);
                    }else{
                        subListItem.appendChild(listItem);
                    }


                    // Trigger infinite children rendering if any
                    if ( v.items && v.items.length > 0 ) {
                        renderChildren(listItem, v.items);
                    }
                }
            }


            // Insert sub menu list items
            if( parent.nodeName === 'LI' ) parent.appendChild(subListItem);
        }

        renderChildren(_nodes.menuBar, options.items);


        // Insert the menu bar

        self.appendChild( _nodes.menuBar );


        /* ----------------------------------------------------------
         * Build the menu bar stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css +=  '\n#menu-bar-' + options.id + ',\n'+
                '#menu-bar-' + options.id + ' *{\n'+
                '    -webkit-box-sizing: border-box;\n'+
                '    -moz-box-sizing: border-box;\n'+
                '    box-sizing: border-box;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + '{\n'+
                '    list-style: none;\n'+
                '    margin: 0;\n'+
                '    padding: 0;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ':before,\n'+
                '#menu-bar-' + options.id + ':after{\n'+
                '    content: \'\';\n'+
                '    display: table;\n'+
                '    clear: both;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li{\n'+
                '    float: left;\n'+
                '    display: inline-block;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' a,\n'+
                '#menu-bar-' + options.id + ' a:visited{\n'+
                '    display: block;\n'+
                '    text-decoration: none;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li > ul{\n'+
                '    position: absolute;\n'+
                '    z-index: -1;\n'+
                '    -webkit-opacity: 0;\n'+
                '    -moz-opacity: 0;\n'+
                '    opacity: 0;\n'+
                '    filter: alpha(opacity=0);\n'+
                '    display: none;\n'+
                '    padding-left: 0;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li > ul:before{\n'+
                '    content:" ";\n'+
                '    position: absolute;\n'+
                '    top: 0;\n'+
                '    right: 0;\n'+
                '    bottom: 0;\n'+
                '    left: 0;\n'+
                '    margin: auto;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li:hover > ul{\n'+
                '    z-index: 2;\n'+
                '    -webkit-opacity: 1;\n'+
                '    -moz-opacity: 1;\n'+
                '    opacity: 1;\n'+
                '    filter: alpha(opacity=1);\n'+
                '    display: block;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li:hover > ul > li{\n'+
                '    z-index: 3;\n'+
                '    position: relative;\n'+
                '}\n';

        css +=  '\n#menu-bar-' + options.id + ' li > ul li{\n'+
                '    float: none;\n'+
                '    display: block;\n'+
                '}\n';


        // User defined stylings
        css += '#menu-bar-' + options.id + '{ ' + rules[0] + '\n}';

        // menu items stylings
        if( rules['items'] ) css += '#menu-bar-' + options.id + ' li{ ' + rules['items'][0] + '\n}';
        if( rules['items'] && rules['items']['first'] ) css += '#menu-bar-' + options.id + ' li:first-child{ ' + rules['items']['first'][0] + '\n}';
        if( rules['items'] && rules['items']['last'] ) css += '#menu-bar-' + options.id + ' li:last-child{ ' + rules['items']['last'][0] + '\n}';
        if( rules['items'] && rules['items']['spacer'] ) css += '#menu-bar-' + options.id + ' li + li{ ' + rules['items']['spacer'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] ) css += '#menu-bar-' + options.id + ' li a{ ' + rules['items']['link'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] ) css += '#menu-bar-' + options.id + ' li a:visited{ ' + rules['items']['link'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] && rules['items']['link']['hover'] ) css += '#menu-bar-' + options.id + ' li a:hover{ ' + rules['items']['link']['hover'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] && rules['items']['link']['hover'] ) css += '#menu-bar-' + options.id + ' li a:visited:hover{ ' + rules['items']['link']['hover'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] && rules['items']['link']['active'] ) css += '#menu-bar-' + options.id + ' li.ui-state-active a{ ' + rules['items']['link']['active'][0] + '\n}';
        if( rules['items'] &&  rules['items']['link'] && rules['items']['link']['active'] ) css += '#menu-bar-' + options.id + ' li.ui-state-active a:visited{ ' + rules['items']['link']['active'][0] + '\n}';
        
        // submenu stylings
        if( rules['submenu'] ) css += '#menu-bar-' + options.id + ' li ul{ ' + rules['submenu'][0] + '\n}';
        
        // first level submenu
        if( rules['submenu'] && rules['submenu']['first-level'] ) css += '#menu-bar-' + options.id + ' > li > ul{ ' + rules['submenu']['first-level'][0] + '\n}';
        
        if( rules['submenu'] && rules['submenu']['icon'] ) css += '#menu-bar-' + options.id + ' li ul:before{ ' + rules['submenu']['icon'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] ) css += '#menu-bar-' + options.id + ' li ul li{ ' + rules['submenu']['items'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['first'] ) css += '#menu-bar-' + options.id + ' li ul li:first-child{ ' + rules['submenu']['items']['first'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['last'] ) css += '#menu-bar-' + options.id + ' li ul li:last-child{ ' + rules['submenu']['items']['last'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['spacer'] ) css += '#menu-bar-' + options.id + ' li ul li + li{ ' + rules['submenu']['items']['spacer'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['link'] ) css += '#menu-bar-' + options.id + ' li ul li a{ ' + rules['submenu']['items']['link'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['link'] ) css += '#menu-bar-' + options.id + ' li ul li a:visited{ ' + rules['submenu']['items']['link'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['link'] && rules['submenu']['items']['link']['hover'] ) css += '#menu-bar-' + options.id + ' li ul li a:hover{ ' + rules['submenu']['items']['link']['hover'][0] + '\n}';
        if( rules['submenu'] && rules['submenu']['items'] && rules['submenu']['items']['link'] && rules['submenu']['items']['link']['hover'] ) css += '#menu-bar-' + options.id + ' li ul li a:visited:hover{ ' + rules['submenu']['items']['link']['hover'][0] + '\n}';
        
        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);

        return _nodes.menuBar;
    };
})();