;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateImageSlide = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {},
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {};

        _nodes.container = this.renderNodeDiv({
            'class' : 'wrap-cycle-slideshow',
            'id' : 'cycle-slider-' + options.id
        });

        _nodes.slide = this.renderNodeUl({
            'class' : 'cycle-slideshow',
            'data-cycle-slides' : '> li',
            'data-cycle-prev' : '#cycle-slider-' + options.id + ' .slide-nav.slide-prev',
            'data-cycle-next' : '#cycle-slider-' + options.id + ' .slide-nav.slide-next',
            'data-cycle-pager' : '#cycle-slider-' + options.id + ' .slide-pager',
            'data-cycle-pager-template' : options.thumbnail ? '<a href=# class=slide-thumbnail><img src={{children.1.src}}></a>' : '<a href=#> {{slideNum}} </a>'
        });

        _nodes.pager = this.renderNodeDiv({
            'class' : 'slide-pager',
        });

        _nodes.nav = {};
        _nodes.nav.prev = this.renderNodeLink({
            'class' : 'slide-nav slide-prev',
        });

        _nodes.nav.next = this.renderNodeLink({
            'class' : 'slide-nav slide-next',
        });
        _nodes.nav.first = this.renderNodeLink({
            'class' : 'slide-nav slide-first',
            'data-cycle-cmd' : 'goto',
            'data-cycle-arg' : 0
        });

        _nodes.nav.last = this.renderNodeLink({
            'class' : 'slide-nav slide-last',
            'data-cycle-cmd' : 'goto',
            'data-cycle-arg' : options.items.length - 1,
            'data-cycle-context' : '#cycle-slider-' + options.id + ' .cycle-slideshow'
        });

        for( var i = 0; i < options.items.length; i++ ){

            var slideItem = this.renderNodeLi(); 
            
            var slideTitle = this.renderNodeText({
                'class' : 'cycle-item-title'
            }); 
            
            var slideImg = this.renderNodeImg({
                'src' : options.items[i].src
            });
            
            var slideZoom = this.renderNodeLink({
                'class' : 'cycle-item-zoom',
                'href' : options.items[i].src,
                'target' : '_blank',
            });

            if( options.items[i].title ) slideTitle.innerHTML = options.items[i].title; 
            if( options.items[i].zoom ) slideZoom.innerHTML = 'zoom';
            
            slideItem.appendChild(slideTitle);
            slideItem.appendChild(slideImg);
            slideItem.appendChild(slideZoom);
            
            _nodes.slide.appendChild(slideItem);

        }

        _nodes.container.appendChild(_nodes.slide);

        if(options.pager) _nodes.container.appendChild(_nodes.pager);
        
        if(options.nav){
            _nodes.container.appendChild(_nodes.nav.prev);
            _nodes.container.appendChild(_nodes.nav.next);
            _nodes.container.appendChild(_nodes.nav.first);
            _nodes.container.appendChild(_nodes.nav.last);
        }


        // Insert the image slider

        self.appendChild( _nodes.container );

        /* ----------------------------------------------------------
         * Build the image slider stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css +=  '\n.wrap-cycle-slideshow *{\n'+
                '    -webkit-box-sizing: border-box;\n'+
                '    -moz-box-sizing: border-box;\n'+
                '    box-sizing: border-box;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow{\n'+
                '    position: relative;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow > .slide-nav{\n'+
                '    cursor: pointer;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow .slide-thumbnail{\n'+
                '    cursor: pointer;\n'+
                '    width: 48px;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow .slide-thumbnail > img{\n'+
                '    display: block;\n'+
                '    max-width: 100%;\n'+
                '    height: auto;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow > .cycle-slideshow,\n'+
                '.wrap-cycle-slideshow > .cycle-slideshow > li{\n'+
                '    display: block;\n'+
                '    width: 100%;\n'+
                '    height: 100%;\n'+
                '    padding: 0;\n'+
                '    margin-top: 0;\n'+
                '    margin-bottom: 0;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow > .cycle-slideshow > li > img{\n'+
                '    display: block;\n'+
                '    width: 100%;\n'+
                '    height: auto;\n'+
                '}\n';

        css +=  '\n.wrap-cycle-slideshow > .cycle-slideshow .cycle-item-title{\n'+
                '    margin-bottom: 0;\n'+
                '    margin-top: 0;\n'+
                '}\n';

        // User defined stylings
        css += '#cycle-slider-' + options.id + '{ ' + rules[0] + '\n}\n';
        
        if( rules['slide'] ) css += '#cycle-slider-' + options.id + ' > ul { ' + rules['slide'][0] + '\n}\n';
        if( rules['items'] ) css += '#cycle-slider-' + options.id + ' > ul > li { ' + rules['items'][0] + '\n}\n';
        if( rules['items'] && rules['items']['title'] ) css += '#cycle-slider-' + options.id + ' > ul > li > .cycle-item-title { ' + rules['items']['title'][0] + '\n}\n';
        if( rules['items'] && rules['items']['zoom'] ) css += '#cycle-slider-' + options.id + ' > ul > li > .cycle-item-zoom { ' + rules['items']['zoom'][0] + '\n}\n';
        if( rules['items'] && rules['items']['zoom'] ) css += '#cycle-slider-' + options.id + ' > ul > li > .cycle-item-zoom:visited { ' + rules['items']['zoom'][0] + '\n}\n';
        if( rules['items'] && rules['items']['zoom'] && rules['items']['zoom']['hover'] ) css += '#cycle-slider-' + options.id + ' > ul > li > .cycle-item-zoom:hover { ' + rules['items']['zoom']['hover'][0] + '\n}\n';
        if( rules['items'] && rules['items']['zoom'] && rules['items']['zoom']['hover'] ) css += '#cycle-slider-' + options.id + ' > ul > li > .cycle-item-zoom:visited:hover { ' + rules['items']['zoom']['hover'][0] + '\n}\n';
        
        if( rules['pager'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager { ' + rules['pager'][0] + '\n}\n';
        if( rules['pager'] && rules['pager']['link'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager > a { ' + rules['pager']['link'][0] + '\n}\n';
        if( rules['pager'] && rules['pager']['link'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager > a:visited { ' + rules['pager']['link'][0] + '\n}\n';
        if( rules['pager'] && rules['pager']['link'] && rules['pager']['link']['hover'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager > a:hover { ' + rules['pager']['link']['hover'][0] + '\n}\n';
        if( rules['pager'] && rules['pager']['link'] && rules['pager']['link']['hover'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager > a:visited:hover { ' + rules['pager']['link']['hover'][0] + '\n}\n';
        if( rules['pager'] && rules['pager']['link'] && rules['pager']['link']['hover'] ) css += '#cycle-slider-' + options.id + ' > .slide-pager > a.cycle-pager-active { ' + rules['pager']['link']['hover'][0] + '\n}\n';
        
        if( rules['nav'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav { ' + rules['nav'][0] + '\n}\n';
        if( rules['nav'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav:visited { ' + rules['nav'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['hover'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav:hover { ' + rules['nav']['hover'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['hover'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav:visited:hover { ' + rules['nav']['hover'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['prev'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav.slide-prev { ' + rules['nav']['prev'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['next'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav.slide-next { ' + rules['nav']['next'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['first'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav.slide-first { ' + rules['nav']['first'][0] + '\n}\n';
        if( rules['nav'] && rules['nav']['last'] ) css += '#cycle-slider-' + options.id + ' > .slide-nav.slide-last { ' + rules['nav']['last'][0] + '\n}\n';

        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);
    };
})();