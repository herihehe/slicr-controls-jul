;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.templateSeekBar = function(settings) {
        var self = this.node,
            /* --------------------------------------
             * Set up default values for the nodes,
             * use original CSS defaults fo reference
             * -------------------------------------- */
            defaults = {
                id: this.getUID(),
                disabled: false,
                styles: {},
                duration: 0, // in seconds
                current: 0 // in seconds
            },
            options = this.extend({}, defaults, settings),
            rules = [],
            css, stylesheet, _nodes = {}, _duration, _current, _remaining;

        // Video duration format
        function secondsToTime(secs)
        {
            secs = Math.round(secs);
            var hours = Math.floor(secs / (60 * 60));

            var divisor_for_minutes = secs % (60 * 60);
            var minutes = Math.floor(divisor_for_minutes / 60);

            var divisor_for_seconds = divisor_for_minutes % 60;
            var seconds = Math.ceil(divisor_for_seconds);

            var obj = {
                "h": hours,
                "m": minutes,
                "s": seconds
            };
            return obj;
        }

        // Number format helper
        function leftPad(number, targetLength) {
            var output = number + '';
            while (output.length < targetLength) {
                output = '0' + output;
            }
            return output;
        }

        _duration = secondsToTime( options.duration );
        _current = secondsToTime( options.current );
        _remaining = secondsToTime( options.duration - options.current );
        
        _nodes.seek = {};
        _nodes.seek.container = this.renderNodeDiv({
            'id': 'seek-' + options.id,
            'class': 'contain-seek'
        });

        _nodes.seek.value = this.renderNodeDiv({
            'class': 'seek-value'
        });

        _nodes.seek.bar = this.renderNodeDiv({
            'class': 'seek-bar'
        });

        _nodes.seek.current = this.renderNodeDiv({
            'class': 'seek-bar-current',
            'style': 'width: ' + ( options.current / options.duration * 100 ) + '%;'
        });

        _nodes.seek.handle = this.renderNodeDiv({
            'class': 'seek-handle',
            'style': 'left: ' + ( options.current / options.duration * 100 ) + '%;'
        });

        _nodes.seek.value.innerHTML = '-' + leftPad( _remaining.m, 2 ) + ':' + leftPad( _remaining.s, 2 );
        _nodes.seek.bar.appendChild( _nodes.seek.current );
        _nodes.seek.bar.appendChild( _nodes.seek.handle );
        _nodes.seek.container.appendChild( _nodes.seek.value );
        _nodes.seek.container.appendChild( _nodes.seek.bar );


        // Insert the seek bar

        self.appendChild( _nodes.seek.container );

        /* ----------------------------------------------------------
         * Build the seek bar stylings based on the template.
         * ---------------------------------------------------------- */
        rules = this.optionsToCss(options.styles);
        
        css = '';

        // Basic stylings
        css += '\n#seek-' + options.id + ' .seek-bar {'+
                '\n    position: relative;'+
                '\n}\n';

        css += '\n#seek-' + options.id + ' .seek-handle {'+
                '\n    position: absolute;'+
                '\n}\n';


        // User defined stylings
        css += '#seek-' + options.id + '{ ' + rules[0] + '\n}';

        if( rules['value'] ) css += '\n#seek-' + options.id + ' .seek-value {\n'+ rules['value'][0] +'\n}\n';
        if( rules['bar'] ) css += '\n#seek-' + options.id + ' .seek-bar {\n'+ rules['bar'][0] +'\n}\n';
        if( rules['bar'] && rules['bar']['current'] ) css += '\n#seek-' + options.id + ' .seek-bar-current {\n'+ rules['bar']['current'][0] +'\n}\n';
        if( rules['bar'] && rules['bar']['handle'] ) css += '\n#seek-' + options.id + ' .seek-handle {\n'+ rules['bar']['handle'][0] +'\n}\n';
        
        
        // insert new stylesheets
        stylesheet = document.createElement('style');
        stylesheet.innerHTML = css;
        this.setAttributes(stylesheet, {
            'type': 'text/css'
        });
        document.getElementsByTagName('head')[0].appendChild(stylesheet);
    };
})();