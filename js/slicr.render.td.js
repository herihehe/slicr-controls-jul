;(function(){
    var slicrMethod = window.slicrMethod || {};
    
    slicrMethod.prototype.renderNodeTd = function(settings) {
        var defaults = {
                id: this.getUID(),
                text: null
            },
            options = this.extend({}, defaults, settings),
            nodeTd;
            
        // Basic template for Td
        nodeTd = document.createElement('td');
        
        // Read user specified settings
        this.readSetting(options, nodeTd);

        return nodeTd;
    };
})();